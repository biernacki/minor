from amuse.units import nbody_system
from amuse.units.optparse import OptionParser

from amuse.ic.plummer import new_plummer_model

from amuse.io import write_set_to_file
from amuse.io import read_set_from_file

import logging
import matplotlib
matplotlib.use('PS')
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
from numpy import linspace, log, std

from progressbar import Bar, ETA, Percentage, ProgressBar
    
def main(N_particles, end_time, resolution, solver):
	
	times = linspace(resolution, end_time, end_time/resolution)

	densities1 = []
	lagrangian_radii1 = []
	radii1 = []
	vel_disp1 = []
	densities2 = []
	lagrangian_radii2 = []
	radii2 = []
	vel_disp2 = []

	if solver == 'Bonsai':
		#folder = '%s_MFruns_N%dk_synced' % (solver, int(N_particles/1024))
		#folder = '%s_MFruns_N%dk_eps0.000000' % (solver, int(N_particles/1024))
		folder = '%s_N%dk_th0.750000' % (solver, int(N_particles/1024))
		#folder = '%s_N%dk_th0.500000' % (solver, int(N_particles/1024))
		lagrangian_radii1, radii1, densities1, vel_disp1 = load_set(resolution, end_time, folder, N_particles)
	elif solver == 'ph4':
		folder = '%s_MFruns_N%dk_eps0.000000' % (solver, int(N_particles/1024))
		lagrangian_radii1, radii1, densities1, vel_disp1 = load_set(resolution, end_time, folder, N_particles)
	else:
		folder1 = 'Bonsai_MFruns_N%dk_synced' % (int(N_particles/1024))
		lagrangian_radii1, radii1, densities1, vel_disp1 = load_set(resolution, end_time, folder1, N_particles)
		folder2 = 'ph4_MFruns_N%dk_eps0.000000' % (int(N_particles/1024))
		lagrangian_radii2, radii2, densities2, vel_disp2 = load_set(resolution, end_time, folder2, N_particles)
	
	
	#part_init = read_set_from_file('initial_cluster_%d.hdf' % N_particles, 'amuse', close_file = True)
	#part_final = read_set_from_file('Bonsai_runs/plummer_%d_t%d.hdf' % (N_particles, int(end_time)), 'amuse', close_file = True)
	
	#plot_view3d(part_init, 'init3d.pdf')
	#plot_view3d(part_final, 'final3d.pdf')
	
	print '\n\nPlease wait - plotting!'
	mf=[0.01, 0.10, 0.5, 0.90]
	
	#c = 15.3
	r_h = lagrangian_radii1[0][2].number
	print 'Half-mass radius:\t%f' % r_h
	t_rh = 0.14*(N_particles*r_h**(1.5))/(log(0.1*N_particles))
	print 'Half-mass relaxation time:\t%f' % t_rh
	
	times /= t_rh
	
	plot_vs_time([densities1,densities2], times, r'$\rho_c$', 'den_%s_%d.eps' % (solver, N_particles), 'Core density, N=%d' % N_particles, solver)
	plot_lr([lagrangian_radii1,lagrangian_radii2], times, mf, 'lr_%s_%d.eps' % (solver, N_particles), 'Lagrangian radii, N=%d' % N_particles, solver)
	plot_vs_time([radii1, radii2], times, r'$R_c$', 'rad_%s_%d.eps' % (solver, N_particles), 'Core radius, N=%d' % N_particles, solver)
#	plot_rhov(vel_disp1, densities1, 'rhov_%s_%d.eps' % (solver, N_particles))

def load_set(resolution, end_time, folder, N_particles):
	
	times = linspace(resolution, end_time, end_time/resolution)
	
	densities = []
	lagrangian_radii = []
	radii = []
	vel_dispersion = []
	
	widgets = ['Reading data: ', Percentage(), ' ', Bar(marker='#'), ' ', ETA(), ' ']
	pbar = ProgressBar(widgets = widgets, maxval = end_time).start()
	
	for t in times:
		input_file = folder + '/plummer_%d_t%04d.hdf' % (N_particles, t)
		
		particles = read_set_from_file(input_file, 'amuse', close_file = True)
		
		#print std(particles.velocity.lengths())
		#print particles.velocity.std()
		
		# getting out number of binaries
		#binaries.append(len(particles.get_binaries(G=nbody_system.G)))
		

		core_centre, core_radius, core_dens = particles.densitycentre_coreradius_coredens(reuse_hop=True)

		#densities.append(particles.cluster_core(reuse_hop=True).density.value_in(nbody_system.mass / nbody_system.length**3))
		densities.append(core_dens.value_in(nbody_system.mass / nbody_system.length**3))

		lr, mf = particles.LagrangianRadii(mf=[0.01, 0.10, 0.5, 0.90],reuse_hop=True)
		lagrangian_radii.append(lr)

		#core_radius = particles.cluster_core(reuse_hop=True).radius.value_in(nbody_system.length)
		radii.append(core_radius.value_in(nbody_system.length))
		#print core_radius

		#core_particles = particles.select(lambda r: (core_centre-r).length() <= core_radius,["position"])
		#print len(core_particles)
		#print core_particles.velocity.std()
		#vel_dispersion.append(((core_particles.velocity.std(axis=0)**2).sum()**0.5).value_in(nbody_system.length / nbody_system.time))
	
		pbar.update(t)
	pbar.finish()
	
	return lagrangian_radii, radii, densities, vel_dispersion
	
		
def plot_vs_time(sth, time, ylabel, savename, title, solver):
	figure = pyplot.figure()
	plot = figure.add_subplot(1,1,1)
	
	if solver == 'Bonsai' or solver == 'ph4':
		plot.plot(time, sth[0], label = solver)
	
	else:
		plot.plot(time, [row for row in sth[0]], label = 'Bonsai')
		plot.plot(time, [row for row in sth[1]], label = 'ph4')
	
	plot.set_yscale('log')
	plot.set_xlabel(u'$t_{rh}$')
	plot.set_ylabel(ylabel)
	plot.set_title(title)
	plot.legend(loc='upper left')
	plot.grid()
	figure.savefig(savename)
	#~ pyplot.show()	

def plot_rhov(vel, rho, savename):
	figure = pyplot.figure()
	plot =  figure.add_subplot(1,1,1)

	plot.plot(log(rho), log(vel))
	plot.set_xlabel(r'$\log \rho_c$')
	plot.set_ylabel(r'$\log v_c^2$')
	figure.savefig(savename)


def plot_lr(radius, time, mf, savename, title,solver):
	figure = pyplot.figure(figsize=(8,8))
	plot = figure.add_subplot(1,1,1)
	
	if solver == 'Bonsai' or solver == 'ph4':
		for item in range(len(mf)):
			plot.plot(time, [row[item].number for row in radius[0]],
					  label = u'%s, %d%%' % (solver, int(mf[item]*100)))
	else: 
		for item in range(len(mf)):
			plot.plot(time, [row[item].number for row in radius[0]],
					  label = u'Bonsai, %d%%' % (int(mf[item]*100)))
		for item in range(len(mf)):
			plot.plot(time, [row[item].number for row in radius[1]],
					  label = u'ph4, %d%%' % (int(mf[item]*100)))

	plot.set_yscale('log')
	plot.set_xlabel(u'$t_{rh}$')
	plot.set_ylabel('Lagrangian radius [N-body system length]')
	plot.set_title(title)
	plot.grid()
	#plot.legend(loc='upper left')
	plot.legend(loc='lower left',prop={'size':8})
	figure.savefig(savename)
	
def plot_view2d(particles, savename):
	figure = pyplot.figure(figsize=(8,8))
	plot = figure.add_subplot(1,1,1)
	
	for body in particles:
		plot.scatter(body.x.value_in(nbody_system.length),
					 body.y.value_in(nbody_system.length),
					 s=5, c='b')
	
	plot.set_xlabel(r'x [N-body length]')
	plot.set_ylabel(r'y [N-body length]')
	#~ plot.set_xlim(-10.0,10.0)
	#~ plot.set_ylim(-10.0,10.0)
	plot.set_title('Positions of particles')
	plot.grid()
	figure.savefig(savename)
	#~ pyplot.show()
	
def plot_view3d(particles, savename):
	figure = pyplot.figure(figsize=(8,8))
	plot = figure.add_subplot(111, projection='3d')
	
	for body in particles:
		plot.scatter(body.x.value_in(nbody_system.length), 
					 body.y.value_in(nbody_system.length), 
					 body.z.value_in(nbody_system.length),
					 s=5, c='b')

	plot.set_xlabel(r'x [N-body length]')
	plot.set_ylabel(r'y [N-body length]')
	plot.set_zlabel(r'z [N-body length]')
	plot.set_xlim(-1000.0,1000.0)
	plot.set_ylim(-1000.0,1000.0)
	plot.set_zlim(-1000.0,1000.0)
	plot.set_title('Positions of particles')
	figure.savefig(savename)
	#~ pyplot.show()

def new_option_parser():
    result = OptionParser()
    
    result.add_option("-N", dest="N_particles", type="int", default = 1024,
                      help="number of particles [%default]")
    result.add_option("-t", dest="end_time", type="int", default = 500,
                      help="end time of the simulation [%default]")
    result.add_option("-r", dest="resolution", type="int", default = 5,
                      help="plot resolution [%default]")
    result.add_option("-s", dest="solver", type="string", default = 'both',
                      help="plot results from solver [%default]")
    return result

if __name__ in ('__main__'):
    o, arguments  = new_option_parser().parse_args()
    main(**o.__dict__)
