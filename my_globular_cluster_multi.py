"""
Evolves an equal mass stellar cluster read from file plummer_8192_t0.hdf
"""

from amuse.units import nbody_system
from amuse.units.optparse import OptionParser

from amuse.community.ph4.interface import ph4
from amuse.community.bonsai.interface import Bonsai
from amuse.community.smalln.interface import SmallN
from amuse.community.kepler.interface import Kepler
from amuse.couple import multiples

from amuse.ic.plummer import new_plummer_sphere

from amuse.io import write_set_to_file
from amuse.io import read_set_from_file

import logging
import os
from sys import exit

#from progressbar import Bar, ETA, Percentage, ProgressBar

import numpy as np

SMALLN = None
def new_smalln():
    SMALLN.reset()
    return SMALLN
    
def init_smalln():
    global SMALLN
    SMALLN = SmallN()
    SMALLN.parameters.timestep_parameter = 0.1
    #SMALLN.parameters.cm_index = 2001
    SMALLN.parameters.cm_index = 400000
    
def init_kepler(star1, star2):        
    try:
        star1.mass.value_in(units.kg) # see if SI units, throw exception if not
        unit_converter \
            = nbody_system.nbody_to_si(star1.mass + star2.mass,
                                       (star2.position-star1.position).length())
    except Exception as ex:
        unit_converter = None
        
    kep = Kepler(unit_converter)
    kep.initialize_code()

    return kep

def print_log(time, gravity, total_energy_at_t0, log_file):
    kinetic_energy = gravity.kinetic_energy
    potential_energy = gravity.potential_energy
    total_energy_at_this_time = kinetic_energy + potential_energy
    print "time                    : " , time
    print "energy error            : " , (total_energy_at_this_time - total_energy_at_t0) / total_energy_at_t0
    
    log_file.write('%04.f\t%.8e\n' % (time.number, ((total_energy_at_this_time - total_energy_at_t0) / total_energy_at_t0)))
    

def simulate_cluster(
        number_of_stars = 2**13, 
        end_time = 2000 | nbody_system.time,
        theta = 0.5,
        solver = 'Bonsai',
        start_file = 'plummer_8192_t0.hdf',       
        ts = 2**16 | nbody_system.time**-1
        ):
		
	# code parameters
	
	timestep = 1.0/ts
	#epsilon = 0.4/number_of_stars | nbody_system.length
	epsilon = 0.0 | nbody_system.length
	dt = 5. | nbody_system.time
	
	init_smalln()
	
	# gravity codes
	if solver == 'Bonsai':
		#gravity = Bonsai(redirection='none')
		gravity = Bonsai()
		gravity.parameters.set_defaults()
		gravity.parameters.timestep = timestep
		gravity.parameters.opening_angle = theta
		gravity.parameters.epsilon_squared = epsilon**2
	    
		print "Using", solver
		print "Opening angle: %f" % gravity.parameters.opening_angle
		print "Smallest timestep: %f" % gravity.parameters.timestep.value_in(nbody_system.time)
		print "Smoothing length: %e" % epsilon.value_in(nbody_system.length)
	
	elif solver == 'ph4':
		gravity = ph4(mode = 'gpu')
		gravity.parameters.set_defaults()
		gravity.parameters.epsilon_squared = epsilon**2
		print "Using", solver
		print "Smoothing length: %e" % epsilon.value_in(nbody_system.length)
	
	
	else:
		print "Wrong solver! Supported: 'bonsai' and 'ph4'\nAborted."
		exit()
	
	
	gravity.particles.collection_attributes.time = end_time
	gravity.particles.collection_attributes.timestep = timestep
	gravity.particles.collection_attributes.epsilon = epsilon
	gravity.particles.collection_attributes.opening_angle = theta
	
	
	
	
	# setting up the system
	if start_file == 'None':
		#np.random.seed(42)
		np.random.seed(23)
		
		cluster = new_plummer_sphere(number_of_stars)
		print cluster.radius[0]	
		file_time = 0.0 | nbody_system.time
	else:
		cluster = read_set_from_file(start_file, 'amuse', close_file = True)
		
		file_time = [int(s) for s in start_file.split('/')[1].split('.')[0].split('_t') if s.isdigit()][0] * 1 | nbody_system.time
		print 'Evolution starting time: ', file_time

	cluster.radius = cluster.mass.number | nbody_system.length # needed for multiples	

	# adding particles
	gravity.particles.add_particles(cluster)
	gravity.commit_particles

	# Bonsai-needed/specific (a tiny bit of evolution required)
	if solver == 'Bonsai':
		gravity.evolve_model(gravity.parameters.timestep)


	# preparing folder for snapshots
	folder = "%s_MFruns_N%dk_synced" % (solver, int(number_of_stars/1024))
	if not os.path.exists(folder):
		os.mkdir(folder)
	if os.path.exists(folder + '/run_log'):
		os.remove(folder + '/run_log')
	log_file = open(folder + '/run_log','w')


	# MULTIPLE >
	stopping_condition = gravity.stopping_conditions.collision_detection
	stopping_condition.enable()
	# <

	# zero-values
	time = 0.0 | nbody_system.time
	total_energy_at_t0 = gravity.kinetic_energy + gravity.potential_energy
	#print_log(time+file_time, gravity, total_energy_at_t0, log_file)

	# MULTIPLE >
	kep = init_kepler(cluster[0], cluster[1])
	multiples_code = multiples.Multiples(gravity, new_smalln, kep)
	# <

	# evolving model
	#pbar = ProgressBar(widgets = ['Progress: ', Percentage(), ' ', Bar(marker='#'), ' ', ETA(), ' '], 
	#				   maxval = (end_time-file_time).number).start()
					   
	while time < (end_time-file_time):
		# snapshot step
		time += dt
		
		# MULTIPLE >
		multiples_code.evolve_model(time)
		# <
		

		print_log(time+file_time, gravity, total_energy_at_t0, log_file)
		write_set_to_file(gravity.particles, folder + "/plummer_%d_t%04d.hdf" % (number_of_stars, (time+file_time).number), 
				"amuse", append_to_file = False)
		
		#pbar.update(time.number)
	
	#~ # final log		
	#~ print_log(time+file_time, gravity, particles, total_energy_at_t0)	
	

	# stopping
	#pbar.finish()
	gravity.stop()
	log_file.close()

	return
	
def new_option_parser():
    result = OptionParser()
    
    result.add_option("-N", dest="number_of_stars", type="int", default = 8192,
                      help="Number of stars [%default]")
    result.add_option("-t", unit=nbody_system.time,
                      dest="end_time", type="float", default = 2000 | nbody_system.time,
                      help="End time of the simulation [%default]")
    result.add_option("-a",
                      dest="theta", type="float", default = 0.5,
                      help="Opening angle [%default]")
    result.add_option("-s",
                      dest="solver", type="string", default = 'Bonsai',
                      help="Solver: [%default]")
    result.add_option("-f",
                      dest="start_file", type="string", default = 'plummer_8192_t0.hdf',
                      help="Starting file: [%default]")
    result.add_option("-i",
                      dest="ts", type="float", default = 2**16 | nbody_system.time**-1,
                      help="Time step: [1./%default]")
    return result

if __name__ in ('__main__'):
	o, arguments  = new_option_parser().parse_args()
	simulate_cluster(**o.__dict__)

