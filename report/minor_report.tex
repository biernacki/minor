\documentclass[a4paper,10pt]{article}

\usepackage[utf8x]{inputenc}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{amsmath}
\usepackage{amsthm} \newtheorem{mytheorem}{Theorem} \theoremstyle{plain}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage[margin=1.2in]{geometry}

\lstset{
numbers=left, 
numberstyle=\tiny, 
stepnumber=1, 
numbersep=5pt, 
language=[ANSI]C,
alsolanguage=Python, 
%basicstyle=\ttfamily, 
breaklines=\True, 
frame=single }



%opening
\title{Minor research project
		\\Core collapse with the Bonsai tree code}
\author{Pawe\l{}~Biernacki\\ \texttt{biernacki@strw.leidenuniv.nl}}


\begin{document}
%<- removes numbers in the bibliography, must sit within the document
\makeatletter
\renewcommand\@biblabel[1]{}
\makeatother
%->
\newcommand{\bonsai}{\texttt{Bonsai}{}}
\newcommand{\ph}{\texttt{ph4}{}}
\newcommand{\Msun}{$M_\odot$}


\maketitle



\section{Introduction}

Globular clusters are old, dynamical systems of hundreds of thousands of stars (see e.g. an overview by \cite{meylanheggie97}). A fate of such a system is well-defined. \textit{Negative heat capacity} (\cite{lyndenbellwood68}) undoubtedly leads to the collapse of the core and its further oscillations. This process happen as the energy of stars in the core is transferred outwards by means of two-body encounters. Time scale related to this process can be described as a time needed for two-body encounters to change orbits of interacting bodies significantly. As an effect velocity distribution of stars is changed toward Maxwellian distribution.

This leads to the slow increase in the core density. High stellar density of the core means close encounters between stars are more frequent and finally binaries form. Energy generated during creation of a binary is then transported outwards.

In the case where the core is sufficiently small, the energy of the halo will not increase enough to stabilize the effect of the increase of the energy in the core. That would result in the increase of the temperature gradient between the two parts of the cluster. Therefore core speeds up its contraction and a runaway occurs (positive feedback); \cite{lyndenbellwood68} coined a term \textit{gravothermal catastrophe} to describe this process.
The effect was confirmed with different analytical methods. \cite{nakada78} and \cite{bettwiesersugimoto84} used gaseous models, while others used Fokker-Planck simulations (e.g. \cite{cohn80}), Monte Carlo simulations (e.g. \cite{gierszheggie93}) and $N$-body simulations (\cite{bettwiesersugimoto85}). 

The core collapse is, however, not an unstoppable process. It can be halted if the core produces more energy that can be transported outwards by a process of two-body relaxation. The oscillatory behaviour after the core collapse was first discovered by means of gaseous models (\cite{bettwiesersugimoto84}) and then later by direct $N$-body simulations (\cite{makino96}).

%In order to perform an analysis one has to define quantities describing properties of globular clusters. It follows from \cite{spitzer87} that
%\begin{equation}
%r_c = \left[ \frac{3v_m(0)^2}{4\pi G\rho(0)} \right]^{1/2}
%\end{equation}
%and $v_m(0)$ is root-mean-square random three-dimensional velocity of stars in the center. %From the assumption that average density in the core of the cluster is half of the central density (\cite{spz97}) the core mass $M_c$ is
%\begin{equation}
%M_c = \frac{2}{3}\pi r_c^3\rho.
%\end{equation}

In this work we have examined the effect of core collapse by means of numerical simulations. We were particularly interested if it can be reproduced by means of gravitational tree-codes. In Section \ref{sec:method} we discuss the numerical method used to solve the problem and shortly explain an idea underlying tree-codes. Furthermore, we discuss setups used to obtain results. Section \ref{sec:results} is devoted to presentation of our results - we first compare our method with direct $N$-body solver and then present outcomes of 32k particles run. In Section \ref{sec:discussion} we conclude on our experiment. Additionally, in Appendix \ref{sec:appendix} we present our implementation of GPU versions of two \textsc{Amuse}/\texttt{bridge} functions and their performance.


\section{Model and Numerical Method}
\label{sec:method}

\subsection{Numerical Method - Tree-code}
\label{sec:tree_code}

% In order to be able to inspect the evolution of any gravitational system one has to integrate the Kepler's %equations of motion. It can be stated mathematically as
%\begin{equation}\label{eq:grav_force}
%m_i\frac{d^2}{dt^2}\vec{r_i}(t) = -G \sum_{j \neq i} \frac{m_i m_j}{|\vec{r_i}-\vec{r_j}|^3}(\vec{r_i}-\vec{r_j}),
%\end{equation}
%where $\vec{r_i}(t)$ is a three-dimensional position vector dependent of $t$, $m_j$ is a mass of of an acting body and $G=6.67\times 10^{-11} m^{3} kg^{-1} s^{-2}$ being Newton's gravitational constant. Therefore, in order to solve Eq. \ref{eq:grav_force} for the system of $N$ particles one have to calculate $(N-1)\times N \times\frac{1}{2}$ accelerations. These factors come from, respectively: number of bodies acting on the chosen one, numbers of bodies in the system and the fact that is symmetric between two interacting objects.

%As the Eq. \ref{eq:grav_force} is a differential equation, one needs to make use of numerical methods in order to solve it. The method (one of few), that gives robust results, is the Hermite integration scheme, which is a predictor-corrector method. The force for a given particle and its derivative are expanded into Taylor series as follows
%\begin{eqnarray}
%F & = & F_0 + F_0't + \frac{1}{2}F_0''t^2 + \frac{1}{6}F_0^{(3)}t^3\\
%F' & = & F_0' + F_0''t + \frac{1}{2}F_0^{(3)}t^2.
%\end{eqnarray}
%Then all particles have their positions and velocities updated as predicted to low order. As new values of $F$ and $F'$ are obtained, then higher order derivatives can be calculated. This procedure leads to expressions for corrections to position $r_i$ and velocity $v_i$ of considered $i$-th particle. For the details of the procedure see e.g. \cite{hutmakinomcmillan95, arseth03}.

Dynamical evolution of the $N$-body system can be obtained by solving a set of differential equations describing forces acting on particles. This can be done with either a direct $N$-body code or a tree-code. An idea of the former is rather simple: we solve for all the forces acting on all particles (which means that computational complexity is of order $O(N^2)$), which is precise, but time computationally expensive. Another way of treating the problem is by the mean of the so-called tree-code, first introduced by \cite{barneshut86}. The complexity is reduced to $O(N\log N)$, as the influence of distant bodies is approximated.

The idea underlying the tree-code method can be explained as follows. Imagine a three-dimensional, cubical space occupied by a set of particles. The space has to be divided in a way, that only a single particle is in a given subcube. Therefore
\begin{enumerate}
\item The initial cube is divided into subcells, each of half width, hight and depth of the \textit{parent} cube.
\item If the \textit{child} cube is empty, we discard it; if the \textit{child} contains exactly one particle we stop division and call that subcell a \textit{leaf}, if however the \textit{child} has more than one particle inside we further divide it to eight cubical subcells (and that \textit{child} become a node).
\item We continue dividing the space as long as we have only one particle per cell; see Figure \ref{fig:2D_division} for an example of such division, which for readability is two-dimensional.
\item Once the space is divided we start creating the \textit{tree} - empty cells are being discarded, while \textit{leafs} connected to their \textit{nodes} and \textit{nodes} with their \textit{nodes} until we reach the \textit{root} level which represents the initial cube. See Figure \ref{fig:tree} for the sketch of the process.
\item At each time step of the computations the \textit{tree} is being reconstructed from the \textit{root}.
\end{enumerate}

\begin{figure}
\centering
\includegraphics[scale=0.8]{2D_division.eps}
\caption{Hierarchical division for the two-dimensional case. Minimum number of subdivisions made, i.e. if the box was empty, no further subdivisions followed. Figure recreated as from \cite{barneshut86}.}
\label{fig:2D_division}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\linewidth]{tree.eps}
\caption{Process of creating a tree. Cells colour mapping: brown - root, pink - node and green - leaf, white - empty.}
\label{fig:tree}
\end{figure}

\par Cell opening criterion is what determines how precise is the computation (and therefore how much of a speed up compared to direct code we achieve). It decides if a cell is sufficiently distant to evaluate forces via multipoles. In others words it describes how deeply should we go away from the root, as the further we go, the more time is used to finish computations. 
%%
The acceptance criterion can be written as
\begin{equation}\label{eq:opening}
d > \frac{l}{\theta} + \delta,
\end{equation}
where $d$ is a distance between a particle (or a group) and the center of mass of other cell, $l$ is a size of the cell on a given level of the tree, $\theta$ is the \textit{opening angle} (which is a free, dimensionless parameter). Additional parameter $\delta$ is a distance between geometrical center and mass center. It helps to avoid error-creating cases when the center of mass is close to cell's edge. See Figure \ref{fig:opening} for a sketch. If opening angle $\theta$ was set to zero, then the direct N-body code should be recovered.

\begin{figure}
\centering
\includegraphics[scale=0.6]{opening.eps}
\caption{Opening criterion. Central point marks geometric center of the cell, cross marks center of the mass, other symbols as described in the text.}
\label{fig:opening}
\end{figure}

In our analysis we have used \bonsai (\cite{bedorf12}), which is a Barnes-Hut algorithm implemented to run fully on GPUs. \texttt{Bonsai} is using the leapfrog predictor-corrector method (see e.g. \cite{hutmakinomcmillan95}). We have run an experimental version of \bonsai, which uses adaptive time step for the code execution. An opening angle we utilized is $\theta=0.5$ for 8k run, which is a rather conservative choice (compared with value used by e.g. \cite{bedorf12}) and $\theta=0.75$ for 32k run (to speed-up computations). Three construction, linking of nodes and leafs, tree traversing due to calculation of forces is computed in parallel for each cell; these result in reduction of a computational time.

We have called the code from within the \textsc{Amuse} framework (\cite{spz09, spz12}). For close encounters between stars we have used fourth-order Hermite integrator, namely \texttt{smallN}. This code, also part of \textsc{Amuse}, is designed to efficiently handle few-body interactions. If the close encounter is discovered, then \bonsai is being paused and \texttt{smallN} being used to resolve that small system. The coupling of codes is being done with the Bridge method (\cite{fujii07}). If a binary was the outcome of such an encounter, then particles were replaced by a binary particle, which had a mass of previously interacting bodies and resultant velocity.

For 8k particles run we have compared our results with the output of a direct code, \texttt{ph4}, which is also available as a part of \textsc{Amuse}. \ph is a 4th order Hermite integrator; see Section \ref{sec:results} for discussion of results.

We denote the time of the core collapse as $t_{cc}$. It can be written that
\begin{equation}
t_{cc} = ct_{rh},\label{eq:t_cc}
\end{equation}
where $t_{rh}$ is half-mass relaxation time (as defined in \cite{spitzer87})
\begin{equation}
t_{rh} = 0.138\frac{r_h^{3/2}N}{G^{1/2}M^{1/2}\ln \Lambda},
\end{equation}
where $r_h$ - half-mass radius, $N$ - number of stars in the cluster, $G$ - gravitational constant, $M$ - total mass of the system and the Coulomb logarithm $\ln \Lambda \approx \ln(0.1N)$. \cite{cohn80} has shown that $c\approx15$.

In case of all runs we were interested in core density $\rho_c$ and core radius $r_c$, as defined by \cite{cassertanohut85}, and Lagrangian radii, especially that of $50\%$ - $r_h$. We have also selected all particles within the core radius and calculated their velocity dispersion $\sigma_v$\footnote{$\sigma_v$ defined as $\sqrt{v_x^2+v_y^2+v_z^2}$}.


\subsection{System of Units and the Initial Conditions}
Throughout our simulations we have adopted the N-body system units, i.e. the total mass of the system $M=1$, the gravitational constant $G=1$ and the total energy of the system $E=-1/4$ (\cite{heggiemathieu86}); this normalization assumes that the system is in virial equilibrium. It can be written that units of mass, length and time are, respectively
\begin{eqnarray}
U_m & = & M\nonumber\\
U_l & = & -\frac{GM^2}{4E}\nonumber\\
U_t & = & \frac{GM^{5/2}}{(-4E)^{3/2}}.\label{eqs:nbodyunits}
\end{eqnarray}
\par We represent globular clusters by the mean of Plummer model \cite{plummer15}. The model was a solution for a polytropic model for a cluster in dynamical equilibrium. Density profile for Plummer model can be written as follows
\begin{equation}
\rho(r) = \frac{3M}{4\pi R^2}\left( 1+\frac{r^2}{R^2} \right)^{-5/2},
\end{equation}
where $M$ is the total mass of a system, $R$ is a scale factor related. Additionally, the radius containing half of the mass, $r_h$ is $1.30R$ (\cite{spitzer87}). Somewhat differently we can define \textit{core radius} $r_c$, i.e. radius at which the surface density $\sigma(r)$ is a half of the value in the center of the cluster, $\sigma(0)$. Additionally, we have assumed that all stars in the examined cluster are of equal mass, i.e. mass of a particle $m=1/N$. We have used an adaptive time step of $1/65536$ in $N$-body system units. Additionally, we examine an idealized case and therefore we do not take into account external tidal field nor the stellar evolution of constituting stars.


\subsection{Hardware and Execution Time}
We have performed our runs on one of the nodes of the Little GREEN Machine (LGM) at Leiden Institute of Advanced Computer Science (LIACS). Each node is equipped with two Intel Xeon E6520 2.4 GHz processors and two Nvidia GeForce GTX480 Graphical Processing Units (GPUs), each with 448 cores of speed 1.215 GHz.
\par The 8k particles run took about 5 days on a single GPU, while 32k run took about 25 days. Certain amount of time was spent on solving two-body systems outside of GPUs and communication between host and device. Additionally, as it can be seen on Fig.~10 of \cite{spz12}, for relatively low number of bodies performance of \bonsai{} is independent of number of particles, which is due to some specific GPU issues (e.g. communication with the device).


\section{Results}
\label{sec:results}

\subsection{Comparison with Direct N-body Solver}
We have compared results of 8k particles runs given by tree code (\texttt{Bonsai}) and direct solver (\texttt{ph4}), see Figure~\ref{fig:lr_both_8192}.
\begin{figure}
\includegraphics[width=0.9\linewidth]{lr_both_8192.eps}
\caption{Comparison of Lagrangian radii evolution for two different \textit{N}-body solvers starting with the same initial conditions.}
\label{fig:lr_both_8192}
\end{figure}
It can be seen that they give qualitatively the same output. However, it can be observed that a cluster evolved with \bonsai collapses earlier and the collapse is not as deep as for the direct code evolution. That can be partially explained by the fact that the core collapse could have been missed while saving outputs. Another explanation lies in the nature of the tree code, which approximates forces from particles farther away, thus possibly leading to earlier collapse. 


\subsection{Analysis of 32k Run}

In the previous section we have shown that we were able to reproduce the core collapse with the tree-code for a relatively small number of particles. Here we describe results for a bigger simulation containing 32768 particles ($2^{15}=32$k).

\begin{figure}
\includegraphics[width=0.9\linewidth]{rad_Bonsai_32768.eps}
\caption{Core radius as a function of half-mass relaxation time for 32k particle run.}
\label{fig:rad_Bonsai_32768}
\end{figure}

\begin{figure}
\includegraphics[width=0.9\linewidth]{den_Bonsai_32768.eps}
\caption{Core density as a function of half-mass relaxation time for 32k particle run.}
\label{fig:den_Bonsai_32768}
\end{figure}

\begin{figure}
\includegraphics[width=0.9\linewidth]{lr_Bonsai_32768.eps}
\caption{Evolution of Lagrangian radii for 32k particle run.}
\label{fig:lr_Bonsai_32768}
\end{figure}

Figure \ref{fig:rad_Bonsai_32768} shows the time evolution of the core radius. We have incorporated the HOP algorithm (\cite{cassertanohut85}) to find that quantity. Figure \ref{fig:lr_Bonsai_32768} presents evolution of Lagrangian radii. Simple comparison of these results with those obtained for 8k particles run reveals few things: first of all the core collapse is shallower in case of 32k particles and secondly, core collapse occurs much later for 32k particles than for 8k particles. It can be estimated that core collapse happened at $t_{cc} \approx 18 t_{rh}$. Therefore, $c$ parameter from eq. \ref{eq:t_cc} is $\sim 18$, in contrast to value found for 8k particles run as well as to value reported by \cite{cohn80}. Perhaps, there is an additional $N$ dependence in the formula describing time of core collapse in terms of $t_{rh}$.

\begin{figure}
\includegraphics[width=\linewidth]{rhov_Bonsai_32768.eps}
\caption{Change in the central velocity dispersion (as defined in the text) with regard to core density - 32k particle run.}
\label{fig:rhov_Bonsai_32768}
\end{figure}

Figure \ref{fig:rhov_Bonsai_32768} depicts relation between the core density and velocity dispersion of particles in the core. It can be seen that it is the same relation as found first in gas-models by \cite{goodman87}) and later in $N$-body simulations by \cite{makino96}. Our definition of velocity dispersion seems to differ from that taken by \cite{makino96}, but shows the same trend. Additionally, it has to be remembered that \cite{makino96} averaged his samples, while we did not (due to lower outputting resolution). It is possible to observe that the track is evolving in a clockwise manner. It can be understood that the core of a cluster is passing through a refrigeration cycle. First, the heat is absorbed in the core (low temperatures) and then released (high temperatures). \cite{makino96} recognized in it the gravothermal expansion; in our case that can be for sure said for 8k particles run, but not for 32k particles run, as we stopped evolving the latter before that became clear.

\section{Discussion}
\label{sec:discussion}

We have performed tree-code based simulations of globular clusters and proved this kind of codes can be successfully used to solve such systems. We made comparison with direct $N$-body solvers and showed our method is leads to qualitatively same results. Additionally, we have run 32k particle simulation which took roughly 25 days on a regular computer equipped with GPU. In the past this kind of performance was only possible on supercomputers or special-purpose computers of GRAPE series (see e.g. discussion in \cite{makino96}).

Further work should be dedicated to finding if the time of core collapse $t_cc$ in terms of the half-mass relaxation time $t_rh$ should not be refined. It appears there is an additional $N$ dependence. In order to examine that more runs for 32k particles as well as for bigger numbers of particles would be required. We have scheduled 1024k (1,048,576) particles run. Perhaps a 256k particles run would be helpful to determine that. However, results of those simulations are going to be available in about 8-10 months for the smaller one and about 1-2 years.

Additionally, we are aware our code lacks tracking of numbers of two-body interactions/binaries formed. It would be helpful to track the post-collapse evolution of cluster. That, however, was beyond the scope of this work, and presented technical difficulties in implementing it.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\

\textit{Acknowledgements} I would like to thank my supervisor Simon Portegies Zwart for an idea of this exciting project and all the guidance. This project would be impossible without the excellent code written by Jeroen B\'{e}dorf, who I am grateful to for his work, time and assistance. Additionally, I would like to thank the \textsc{Amuse} team - especially Inti Pelupessy and Nathan de Vries. I also thank Steven Rieder and Tjarda Boekholt for stimulating discussions.

%\bibliographystyle{plain}
%\bibliography{core}

\begin{thebibliography}{}
  \bibitem[\textsc{Amuse} Team]{amuse} \textsc{Amuse} Team, \textsc{Amuse} code, \url{http://amusecode.org}.
%  \bibitem[Areseth (2003)]{arseth03} Aarseth S. J., 2003, Cambridge University Press.
  \bibitem[Barnes \& Hut (1986)]{barneshut86} Barnes J. \& Hut P., 1986, Nature 324.
%  \bibitem[Baumgardt et al. (2008)]{baumgardt08} Baumgardt H. et al., 2008, arXiv:astro-ph/0301166v2.	
  \bibitem[B\'edorf, Gaburov \& Portegies Zwart (2012)]{bedorf12} B\'edorf J., Gaburov E. \& Portegies Zwart S., 2012, JCoPh, 231, 2825.
  \bibitem[Bettwieser (1983)]{bettwieser83} Bettwieser E., 1983, MNRAS, 203, 811-831.
  \bibitem[Bettwieser \& Sugimoto (1984)]{bettwiesersugimoto84} Bettwieser E., Sugimoto D., 1984, MNRAS, 208, 493-509.
  \bibitem[Bettwieser \& Sugimoto (1985)]{bettwiesersugimoto85} Bettwieser E., Sugimoto D., 1985, MNRAS, 212, 189-195.
  \bibitem[Cassertano \& Hut (1985)]{cassertanohut85} Cassertano S. \& Hut P., 1985, ApJ 298, 90-94.
  \bibitem[Cohn (1980)]{cohn80} Cohn H., 1980, ApJ 242, 765-771.
  \bibitem[Eisenstein \& Hut (1998)]{eisensteinhut88} Eisenstein D.J. \& Hut P., 1998, ApJ 498.
%  \bibitem[Elson, Hut \& Inagaki (1987)]{elsonhutinagaki87} Elson R., Hut P. \& Inagaki S., 1987, Ann. Rev. Astron. Astrophys.
  \bibitem[Fujii et al. (2007)]{fujii07} Fujii M.S. et al., 2007, PASJ 59, 1095.
%  \bibitem[Fujii \& Portegies Zwart (2013)]{fujiispz13} Fujii M.S. \& Portegies Zwart S., 2013, submitted to MNRAS, arXiv:1304.1550.
  \bibitem[Giersz \& Heggie (1993)]{gierszheggie93} Giersz M. \& Heggie D.C., 1994, arXiv:astro-ph/9305008v1.
%  \bibitem[Giersz \& Heggie (1994)]{gierszheggie94} Giersz M. \& Heggie D.C., 1994, arXiv:astro-ph/9403024v1.
  \bibitem[Goodman (1987)]{goodman87} Goodman J., 1987, ApJ, 313, 576-595.
  \bibitem[Heggie \& Mathieu (1986)]{heggiemathieu86} Heggie D.C. \& Mathieu R.D., 1986, The Use of Supercomputers in Stellar Dynamics, doi:10.1007/BFb0116419.
  \bibitem[Hut, Makino \& McMillan (1995)]{hutmakinomcmillan95} Hut P., Makino J., McMillan S., 1995, ApJL 443, L93-L96.
%  \bibitem[Hut (1998)]{hut98} Hut P., 1998, arXiv:astro-ph/9804089v1.
  \bibitem[Kl\"{o}ckner A. et al. (2008)]{klockner08} Kl\"{o}ckner A. et al., 2011, submitted to Parallel Computing, Elsevier, arXiv:0911.3456.
  \bibitem[Lynden-Bell \& Wood (1968)]{lyndenbellwood68} Lynden-Bell D. \& Wood R., 1968, MNRAS 138, 495-525.
  \bibitem[Makino (1996)]{makino96} Makino J., 1996, ApJ 471, 796-803.
%  \bibitem[Makino \& Aarseth (1992)]{makinoaarseth92} Makino, J. \& Aarseth, S., 1992, PASJ 44, 141-151.
  \bibitem[Meylan \& Heggie (1997)]{meylanheggie97} Meylan G. \& Heggie D.C., 1997, A\&AR, doi:10.1007/s001590050008.
  \bibitem[Nakada (1978)]{nakada78} Nakada Y., 1978, 30, 57-66.
  \bibitem[Plummer (1915)]{plummer15} Plummer H.C., 1915, MNRAS 76, 107–121.
  \bibitem[Portegies Zwart et al. (1997)]{spz97} Portegies Zwart S., Hut, P. \& Verbunt, F., 1997, A\&A 328, 130-142.
  \bibitem[Portegies Zwart et al. (2009)]{spz09} Portegies Zwart S. et al., 2009, New Astronomy, Vol. 14, Issue 4, 369-378.
  \bibitem[Portegies Zwart et al. (2012)]{spz12} Portegies Zwart S. et al., 2012, arXiv:1204.5522.
  \bibitem[Spitzer (1987)]{spitzer87} Spitzer L.Jr., 1987, Princeton University Press.
%  \bibitem[Spitzer \& Hart (1971)]{spitzerhart71} Spitzer, L.Jr. \& Hart, M., 1971, ApJ 164, 399-409.

\end{thebibliography}

\appendix 
\section{PyCUDA versions of \textsc{Amuse} \texttt{bridge} functions}
\label{sec:appendix}
In the world of astrophysical simulations time and effectiveness of computations are absolutely crucial. Even if a speed of computers doubles every 18 months, better and smarter solutions are always welcomed. \textsc{Amuse}, the Astrophysical MUltipurpose Software Environment, is a solution which provides sophisticated computational solutions to curious astrophysicists. Implemented in Python, allows access to different existing numerical codes from the level of user scripts.
\par Number of required operations in $N$-body simulations puts high demands on computational efficiency. This can be achieved e.g. by use of Graphical Processing Units (GPUs), which can perform many (same) computations simultaneously. This is possible with parallel computing platforms such as Nvidia's CUDA \footnote{\url{http://www.nvidia.com/object/cuda_home_new.html}} or Khronos Group's OpenCL \footnote{\url{http://www.khronos.org/opencl}}.
\par GPU card (usually referred to as \textit{device}) can be instructed directly through CUDA or through a Python wrapper (named PyCUDA). In the first case user is asked to perform all memory management on his own, while the latter completes those automatically hiding all 'gory' details. Nevertheless, a \textit{kernel}, i.e. a function which runs on the device, have to be written in C, not Python.
\par In this Appendix we present GPU implementations of two functions used in \textsc{Amuse} and resulting speed up.

\subsection{PyCUDA implementations of \texttt{get\_gravity\_at\_point} and \texttt{get\_potential\_at\_point}}

As an example of our implementation we will discuss in detail differences between \texttt{get\_potential\_at\_point} and its PyCUDA counterpart. Crucial (computational) parts of aforementioned functions are shown below as snippets.

\begin{itemize}
\item old \texttt{bridge} implementation

In the classical case all the computations are performed for a given particle \texttt{i} with the respect to all particles in the set/array. There is one computation performed at the time; see Listing \ref{listing:old_bridge} below:

\begin{lstlisting}[caption={Old \texttt{bridge} implementation}, label={listing:old_bridge}]
for i in range(len(x)):
  dx = x[i] - positions.x
  dy = y[i] - positions.y
  dz = z[i] - positions.z
  dr_squared = (dx * dx) + (dy * dy) + (dz * dz)
  dr = (dr_squared + self._softening_lengths_squared()).sqrt()
  energy_of_this_particle = (self.particles.mass / dr).sum()
  result.append(-self.gravity_constant * energy_of_this_particle)
\end{lstlisting}


\item PyCUDA implementation

We define the kernel as a string, which is then being interpreted by PyCUDA \texttt{SourceModule} class and its parameter \texttt{get\_function}, please, see Listing \ref{listing:cuda_code} below. The keyword \texttt{\_\_global\_\_} is informing the compiler that the following function is going to be a kernel. The set of particles is divided into blocks of the size acceptable by PyCUDA (computationally most convenient are powers of 2; in our case $2^8$). Each thread on the GPU is computing potential for one particle given by the index \texttt{i} with respect to all the rest particles (index \texttt{j}); $n$ computations are being performed in the same time. The \texttt{if} statement is taking care of situation when particles have the same position and the smoothing parameter is not specified. 
\pagebreak
\begin{lstlisting}[caption={PyCUDA implementation}, label={listing:cuda_code}]
module = SourceModule("""
__global__ void gpap(float *mass, 
		float *xx, float *yy, float *zz, 
		float *x, float *y, float *z, 
		float *eps2, float *phi,  int *N)
{
    int i = blockIdx.x*blockDim.x+threadIdx.x;
    int j;
    phi[i]=0;    
    float inv_dr;
    
    for (j=0;j<*N;j++)
    {
        inv_dr = rsqrt((xx[j]-x[i])*(xx[j]-x[i])
    			+(yy[j]-y[i])*(yy[j]-y[i])
        		+(zz[j]-z[i])*(zz[j]-z[i])
        		+*eps2); 
        
        if (inv_dr == NAN)
        {
            phi[i] += 0.0;
        }
        else
        {
            phi[i] += -mass[i]*inv_dr;
        }
    }
}
""")

        gpap = module.get_function("gpap")
\end{lstlisting}

\end{itemize}

\subsection{Performance comparison}
\begin{figure}[t]
\includegraphics[width=\linewidth]{comp_loglog.eps}
\caption{Comparison of timings of \texttt{get\_gravity\_at\_point} (get\_gravity) and \texttt{get\_potential\_at\_point} (get\_potential) in old \texttt{bridge} and new PyCUDA \texttt{bridge}. Please note logarithmic axis.}
\label{fig:ggap+gpap_comp_loglog}
\end{figure}

Computations of both gravitational potential and force were performed for PyCUDA and old \texttt{bridge} version. Particles were following the Plummer distribution (see e.g. \cite{plummer15}). The only parameter differing between two runs of simulations was the number of particles (with a starting value of $2^{8}$ then doubled in each run up to $2^{17}$ in the last one). Please, see Figure \ref{fig:ggap+gpap_comp_loglog} for graphical comparison. One can clearly notice that PyCUDA implementation of \texttt{get\_gravity} is about 50 times faster and \texttt{get\_potential} about 13 times than their pythonic counterparts. This is a substantial speed-up of computations.


\end{document}
